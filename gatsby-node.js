const path = require('path');

module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const res = await graphql(`
    query {
      allProjectsJson {
        edges {
          node {
            slug
            title
            url
            repo
            layout
            image 
            description
            tags {
              tag
              id
            }
          }
        }
      }
    }
  `);

  const layouts = ['primary', 'secondary'];
  res.data.allProjectsJson.edges.forEach((project) => {
    const projectTemplate = path.resolve(
      `./src/components/templates/${
        layouts.includes(project.node.layout) ? project.node.layout : 'primary'
      }.tsx`,
    );
    createPage({
      path: `/projects/${project.node.slug}/`,
      component: projectTemplate,
      context: { project },
    });
  });
};
