import React from 'react';

import Footer from '../Footer';
import LayoutPrimary from './LayoutPrimary';

interface Props {
  pageContext: any;
}

const primary: React.FC<Props> = ({ pageContext }) => {
  const { project } = pageContext;
  return (
    <>
      <LayoutPrimary project={project} />
      <Footer />
    </>
  );
};

export default primary;
