import React from 'react';
import { Link } from 'gatsby';

import Url from '../../components/Url';

interface Props {
  project: any;
}

const LayoutPrimary: React.FC<Props> = ({ project }) => {
  const { title, slug, description, tags, repo, image } = project.node;
  return (
    <div className='container mx-auto'>
      <h1 className='my-11 text-7xl text-gray-100 font-semibold text-center'>{title}</h1>
      <div className='grid grid-cols-1 md:grid-cols-2 mx-6'>
        <img src={image} alt={title} />
        <div>
          <p className='text-gray-300 my-4'>{description}</p>
          <Url url={repo} name='View repository' />
          <p className='font-semibold text-gray-100 mt-4'>Tags:</p>
          <p className='font-normal text-gray-200'>{` ${tags
            .map((tag: any) => tag.tag)
            .join(', ')}`}</p>
          <p className='font-light text-gray-300 mb-11 mt-11'>{slug}</p>
          <Link to='/projects'>
            <p className='text-gray-100 font-semibold'>Go back</p>
          </Link>
        </div>
      </div>
    </div>
  );  
};

export default LayoutPrimary;
