import React, { useMemo } from 'react';

const Footer: React.FC = () => {
  const currentYear = () => new Date().getFullYear();
  const year = useMemo(() => currentYear(), [currentYear]);
  return <footer className='text-center text-gray-200 my-11'>Julio Canizalez &copy; {year} Copyright All Rights Reserved</footer>;
};

export default React.memo(Footer);
