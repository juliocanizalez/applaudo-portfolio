import React from 'react';

interface Props {
  title: string;
  content: string;
  year: string;
}

const InformationItem: React.FC<Props> = ({ title, content, year }) => {
  return (
    <>
      <h1 className='text-2xl font-medium text-gray-200'>{title}</h1>
      <h4 className='text-gray-400 mt-2 mb-3'>{year}</h4>
      <p className='text-gray-300 mb-6'>{content}</p>
    </>
  );
};

export default InformationItem;
