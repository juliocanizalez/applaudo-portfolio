import React from 'react';

import InformationItem from './InformationItem';

const Experience: React.FC = () => {
  return (
    <div className='mx-11 mt-11'>
      <h1 className='text-gray-50 text-extrabold text-3xl mb-11'>Experience</h1>
      <InformationItem title='Trainee' year='2021' content='Applaudo Studios' />
      <InformationItem title='Web Developer' year='2019' content='Red Avanzada para la Investigación, Ciencia y Educación Salvadoreña (RAICES)' />
      <InformationItem title='IT Support' year='2016 - 2019' content='Mission of God Academy' />
    </div>
  );
};

export default Experience;
