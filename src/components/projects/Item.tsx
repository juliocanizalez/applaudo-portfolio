import React from 'react';
import { Link } from 'gatsby';

interface Props {
  image: string;
  title: string;
  slug: string;
}

const Item: React.FC<Props> = ({ image, title, slug }) => (
  <div className='bg-gray-800 p-4'>
    <Link to={`/projects/${slug}`}>
      <img src={image} alt={title} />
      <h2 className='text-gray-100 font-semibold mt-3'>{title}</h2>
      <p className='text-gray-300'>View More</p>
    </Link>
  </div>
);

export default Item;
