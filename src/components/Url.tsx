import React from 'react';

interface Props {
  url: string;
  name: string;
}

const Url: React.FC<Props> = ({ url, name }) => (
  <a
    href={url}
    className='text-gray-400 mr-3 text-lg font-medium hover:text-gray-100'
    target='_blank'
    rel='noreferrer nofollow'
  >
    {name}
  </a>
);

export default Url;
