import React from 'react';

import InformationItem from './InformationItem';

const Education: React.FC = () => {
  return (
    <div className='mx-11 mt-11'>
      <h1 className='text-gray-50 text-extrabold text-3xl mb-11'>Education</h1>
      <InformationItem
        title='Software Development Engineer'
        content='Universidad Católica de El Salvador'
        year='2018 - 2023'
      />
      <InformationItem
        title='System Engineering Technician'
        content='ITCA-FEPADE'
        year='2014 - 2016'
      />
    </div>
  );
};

export default Education;
