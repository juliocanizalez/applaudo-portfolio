import React from 'react';
import { Player, Controls } from '@lottiefiles/react-lottie-player';

import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import Experience from '../components/Experience';
import Education from '../components/Education';
import { Helmet } from 'react-helmet';

const index = () => (
  <>
    <Helmet title='Julio Canizalez' defer={false} />
    <Navbar />
    <main className='bg-gray-900 container mx-auto'>
      <div className='grid grid-cols-1 md:grid-cols-5 my-11 mx-6'>
        <section className='md:col-span-2 self-center'>
          <h1 className='text-7xl text-gray-100 font-semibold text-center md:text-left'>
            Hello, I'm Julio Canizalez
          </h1>
          <p className='my-12 text-gray-300'>
            React Developer with a few experiences in roles like IT Support, PHP Developer and CMS
            management. Always looking for new challenges in the web development to apply my
            experience and skills.
          </p>
        </section>
        <section className='md:col-span-3 overflow-y-auto max-80'>
          <Player
            autoplay
            loop
            style={{ width: '80%', height: 'auto' }}
            src='https://assets4.lottiefiles.com/packages/lf20_n9ryrmts.json'
          >
            <Controls visible={false} />
          </Player>
          <Experience />
          <Education />
        </section>
      </div>
    </main>
    <Footer />
  </>
);

export default index;
