import React from 'react';
import { Player, Controls } from '@lottiefiles/react-lottie-player';

import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import Url from '../components/Url';
import { Helmet } from 'react-helmet';

const contact = () => {
  return (
    <>
      <Helmet title='Contact' defer={false} />
      <Navbar />
      <section className='bg-gray-900 container mx-auto'>
        <div className='grid grid-cols-1 md:grid-cols-5 my-11 mx-6'>
          <section className='col-span-2 self-center text-center'>
            <h1 className='text-7xl text-gray-100 font-semibold text-center md:text-left'>
              Contact Me
            </h1>
            <p className='my-12 text-gray-300'>Find me on</p>
            <div>
              <Url name='Gitlab' url='https://gitlab.com/juliocanizalez' />
              <Url name='Github' url='https://github.com/juliocanizalez' />
              <Url name='LinkedIn' url='https://www.linkedin.com/in/juliocanizalez/' />
            </div>
          </section>
          <section className='col-span-3'>
            <Player
              autoplay
              loop
              style={{ width: '80%', height: 'auto' }}
              src='https://assets1.lottiefiles.com/packages/lf20_bp1bwvhv.json'
            >
              <Controls visible={false} />
            </Player>
          </section>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default contact;
