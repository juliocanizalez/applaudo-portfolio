/* eslint-disable */
import { graphql } from 'gatsby';
import React, { useState, useEffect } from 'react';
import { useStaticQuery } from 'gatsby';
import arr from 'lodash';

import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import Item from '../components/projects/Item';
import { Helmet } from 'react-helmet';

const options = [
  { option: 'All' },
  { option: 'Js' },
  { option: 'Css' },
  { option: 'Design Patterns' },
  { option: 'React' },
  { option: 'Redux' },
  { option: 'Typescript' },
  { option: 'Linter' },
  { option: 'Scss' },
  { option: 'Gatsby' },
  { option: 'Jest' },
  { option: 'React Testing Library' },
];

const projects = () => {
  const query = useStaticQuery(graphql`
    query {
      allProjectsJson {
        edges {
          node {
            slug
            title
            url
            repo
            layout
            image
            description
            tags {
              tag
              id
            }
          }
        }
      }
    }
  `);
  const [projects, setProjects] = useState(query.allProjectsJson.edges);
  const [inputValue, setInputValue] = useState('');

  const handleSelect = () => {
    if (inputValue !== '' && inputValue !== 'All') {
      const filteredProjects = arr.filter(query.allProjectsJson.edges, {
        node: { tags: [{ tag: inputValue }] },
      });
      setProjects(filteredProjects);
    } else {
      setProjects(query.allProjectsJson.edges);
    }
  };

  useEffect(() => {
    handleSelect();
  }, [inputValue]);

  const handleChange = (e: any) => {
    setInputValue(e.target.value);
  };

  return (
    <>
      <Helmet title='Projects' defer={false} />
      <Navbar />
      <main className='container mx-auto bg-gray-900'>
        <div className='my-11 mx-6'>
          <h1 className='text-center text-7xl text-gray-100 font-semibold'>Projects</h1>
          <div className='mt-11'>
            <p className='mt-11 inline text-gray-200 mr-4'>Filter by</p>
            <select onChange={handleChange} className='bg-gray-700 text-gray-100 py-1 px-3 outline-none'>
              <option selected disabled>
                Select an option
              </option>
              {options.map((item) => (
                <option value={item.option}>{item.option}</option>
              ))}
            </select>
          </div>
          <div className='grid gap-4 grid-cols-2 md:grid-cols-5 mt-6'>
            {projects.map((project: any) => (
              <Item
                title={project.node.title}
                image={project.node.image}
                slug={project.node.slug}
              />
            ))}
          </div>
        </div>
      </main>
      <Footer />
    </>
  );
};

export default projects;
