import React from 'react';
import { Player, Controls } from '@lottiefiles/react-lottie-player';
import { Helmet } from 'react-helmet';

const NotFound = () => (
  <>
    <Helmet title='Not Found' defer={false} />
    <div className='bg-gray-900'>
      <div className='container mx-auto h-screen grid place-items-center'>
        <h3 className='text-2xl font-semibold text-center self-end text-gray-50'>
          The requested page doesn't exists!
        </h3>
        <Player
          autoplay
          loop
          style={{ width: '80%', height: 'auto' }}
          src='https://assets5.lottiefiles.com/private_files/lf30_3X1oGR.json'
        >
          <Controls visible={false} />
        </Player>
      </div>
    </div>
  </>
);

export default NotFound;
