module.exports = {
  rules: {
    'no-anonymous-exports-page-templates': 'warn',
    'limited-exports-page-templates': 'warn',
    'no-console': 'warn',
    'jsx-quotes': ['error', 'prefer-single'],
    semi: ['error', 'always'],
    quotes: ['error', 'single'],
  },
};
